	### ALIASES ###
alias grep='grep --color=auto'

alias sd='shutdown now'
alias zz='systemctl suspend'
alias zzz='systemctl suspend'
alias rb='reboot'

alias ls='eza -lh'
alias lsl='eza -lah'
alias lsa='eza -lah'
alias lss='eza'

alias ud='yay && flatpak update'

alias dl='transmission-cli'
alias torrent='cd Downloads && transmission-cli *.torrent && echo "organise your downloads!"'

alias pyconf='nvim ~/.config/qtile/config.py'
alias qtile_conf='nvim ~/.config/qtile/config.py'

alias and='&&'
alias AND='&&'

alias runelite='./Downloads/runelite/RuneLite.AppImage'
alias rl='./Downloads/runelite/RuneLite.AppImage'

alias clear='clear && macchina'

