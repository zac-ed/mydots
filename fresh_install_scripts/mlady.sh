#/bin/bash

	### For virtualisation, gui file explorer, appearance, ###
	###   archiving,   neovim setup,  package management   ###
sudo dnf upgrade
sudo dnf install neovim qtile rofi alacritty zip lxappearance qt5ct p7zip xclip ristretto virt-manager bridge-utils qemu libguestfs netcat ebtables libguestfs libvirt cargo flatpak nodejs npm lua eza xset

flatpak install brave

cargo install macchina --locked

	### Starship prompt ###
curl -sS https://starship.rs/install.sh | sh

cp -r nvim qtile macchina ~/.config
cp starship.toml ~/.config
cp .xinitrc .bash* .alacritty.toml ~
sudo cp rosy.rasi /usr/share/themes/rofi
cp rosy.rasi ~

	### Get Agave Nerd Fonts ###
url="https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/Agave.zip"

curl -LJO "$url"

if [ $? -eq 0 ]; then
  unzip Agave.zip
  sudo mv *tf /usr/share/fonts
  fc-cache -fv
else
  echo "Failed to download agave nerd font!"
fi

	### Add self to groups! ###
groups=("kvm" "qemu" "libvirt" "video" "audio" "input")

for group in "${groups[@]}"; do
    sudo usermod -a -G "$group" zaced 
done

	###  Enable virtualisation services! ###
sudo systemctl start libvirtd
sudo systemctl enable libvirtd
sudo virsh net-autostart default

reboot
