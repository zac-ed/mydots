#!/bin/bash

sudo zypper install git rofi picom feh caja zip unzip lxapearance qt5ct p7zip xclip ristretto firefox qemu-full ebtables libguestfs libvirt openbsd-netcat vde2 virt-manager ufw python311-pip python311-pipx cargo flatpak fontconfig-devel gcc cmake gcc-c++ nodejs21 npm21 xorg-x11-server xinit

cargo install eza
cargo install alacritty
cargo install macchina --locked

pipx install qtile

curl -sS https://starship.rs/install.sh | sh

cp -r qtile nvim macchina ~/.config
cp picom.conf starship.toml ~/.config
cp -r bin ~/
cp .alac* .bash* ~/
cp .xinitrc.ahoy ~/.xinitrc
sudo cp eggbox* /usr/share/themes/rofi
cp eggbox* ~

	### Get Agave Nerd Font Mono ###
url="https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/Agave.zip"

curl -LJO "$url"

if [ $? -eq 0 ]; then
  unzip Agave.zip
  sudo mv *tf /usr/share/fonts
else
  echo "Failed to download agave nerd font!"
fi

sudo systemctl start libvirtd
sudo systemctl enable libvirtd
sudo virsh net-autostart default

groups=("kvm" "qemu" "libvirt" "video" "audio" "input")

for group in "${groups[@]}"; do
    sudo usermod -aG "$group" zaced
done

echo "visit this link: https://www.gnome-look.org/p/1310034/ for your icon pack"
echo "visit this link: https://www.gnome-look.org/p/1276216 for your GTK theme"

